require_relative '../uploaders/video_uploader'

class ConversionRequest < Hanami::Entity
  include VideoUploader[:video]

  attributes do
    attribute :id, Types::Int
    attribute :job_id, Types::String
    attribute :video_data, Types::String
  end
end
