require 'shrine'
require 'shrine/storage/file_system'
require 'streamio-ffmpeg'

Shrine.storages = {
  cache: Shrine::Storage::FileSystem.new('public', prefix: 'uploads/cache'), # temporary
  store: Shrine::Storage::FileSystem.new('public', prefix: 'uploads/store'), # permanent
}

Shrine.plugin :versions

unless Hanami.env == 'test'
  Shrine.plugin :hanami
  Shrine.plugin :backgrounding
  Shrine::Attacher.promote do |data|
    ShrinePromotionWorker.perform_async(data)
  end
end

class VideoUploader < Shrine
  plugin :hanami
  plugin :processing
  plugin :versions

  process(:store) do |io, _context|
    mov        = io.download
    video      = Tempfile.new(['video', '.mp4'], binmode: true)
    screenshot = Tempfile.new(['screenshot', '.jpg'], binmode: true)

    movie = FFMPEG::Movie.new(mov.path)
    movie.transcode(video.path)
    movie.screenshot(screenshot.path)

    mov.delete

    { video: video, screenshot: screenshot }
  end
end
