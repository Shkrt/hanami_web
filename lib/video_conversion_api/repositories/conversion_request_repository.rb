require_relative '../uploaders/video_uploader'

class ConversionRequestRepository < Hanami::Repository
  prepend VideoUploader.repository(:video)
end
