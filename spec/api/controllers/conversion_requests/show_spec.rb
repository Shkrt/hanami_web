require 'spec_helper'
require_relative '../../../../apps/api/controllers/conversion_requests/show'

describe Api::Controllers::ConversionRequests::Show do
  let(:action) { Api::Controllers::ConversionRequests::Show.new }
  let(:video_file) { Rack::Test::UploadedFile.new(Hanami.root.join('spec', 'fixtures', 'small.mp4')) }
  let(:conv_request) { ConversionRequestRepository.new.create(ConversionRequest.new(video: video_file)) }
  let(:params) { Hash[id: conv_request.id] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
