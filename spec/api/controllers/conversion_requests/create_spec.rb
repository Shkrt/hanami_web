require 'spec_helper'
require_relative '../../../../apps/api/controllers/conversion_requests/create'

describe Api::Controllers::ConversionRequests::Create do
  let(:action) { Api::Controllers::ConversionRequests::Create.new }
  let(:video_file) { Rack::Test::UploadedFile.new(Hanami.root.join('spec', 'fixtures', 'small.mp4')) }
  let(:params) { Hash[conversion_request: { video_data: { tempfile: video_file }}] }

  it 'is successful' do
    response = action.call(params)
    last_conv_request = ConversionRequestRepository.new.last
    response[0].must_equal 200
    action.exposures.must_include(:conv_request)
  end
end
