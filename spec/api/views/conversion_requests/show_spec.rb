require 'spec_helper'
require_relative '../../../../apps/api/views/conversion_requests/show'

describe Api::Views::ConversionRequests::Show do
  let(:conv_request) { ConversionRequest.new(id: 55) }
  let(:exposures) { Hash[conv_request: conv_request] }
  let(:view)      { Api::Views::ConversionRequests::Show.new(nil, exposures) }
  let(:rendered)  { view.render }

  it 'exposes #conversion_request' do
    json = JSON.parse rendered
    json.must_equal({'id' => 55})
  end
end
