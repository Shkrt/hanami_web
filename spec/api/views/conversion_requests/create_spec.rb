require 'spec_helper'
require_relative '../../../../apps/api/views/conversion_requests/create'

describe Api::Views::ConversionRequests::Create do
  let(:conv_request) { ConversionRequest.new(id: 1, job_id: '3d2323') }
  let(:exposures) { Hash[conv_request: conv_request] }
  let(:view)      { Api::Views::ConversionRequests::Create.new(nil, exposures) }
  let(:rendered)  { view.render }

  it 'exposes conv_request' do
    json = JSON.parse rendered
    json.must_equal({'id' => 1})
  end
end
