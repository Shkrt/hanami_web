# Configure your routes here
# See: http://hanamirb.org/guides/routing/overview/
#
get '/conversion_requests/:id', to: 'conversion_requests#show'
post '/conversion_requests', to: 'conversion_requests#create'
