module Api::Controllers::ConversionRequests
  class Create
    include Api::Action

    expose :conv_request

    def call(params)
      tempfile = params[:conversion_request][:video_data][:tempfile]
      new_request = ConversionRequest.new(video: ::File.open(tempfile))
      @conv_request = ConversionRequestRepository.new.create(new_request)
    end
  end
end
