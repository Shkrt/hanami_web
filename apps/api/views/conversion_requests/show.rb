module Api::Views::ConversionRequests
  class Show
    include Api::View
    layout false

    def render
      _raw(JSON.pretty_generate conv_request.to_h.select { |k, v| attributes.include? k })
    end

    private

    def attributes
      %i(id)
    end
  end
end
