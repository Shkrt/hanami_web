Hanami::Model.migration do
  change do
    create_table :conversion_requests do
      primary_key :id

      column :job_id, String
      column :video_data, 'text'

      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false
    end
  end
end
